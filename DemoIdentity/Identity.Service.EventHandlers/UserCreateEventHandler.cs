﻿using Identity.Domain;
using Identity.Service.EventHandlers.Command;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;


namespace Identity.Service.EventHandlers
{
    public class UserCreateEventHandler : IRequestHandler<UserCreateCommand, IdentityResult>
    {
        private readonly ILogger<UserCreateEventHandler> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        
        public UserCreateEventHandler(
            ILogger<UserCreateEventHandler> logger,
            UserManager<ApplicationUser> userManager)
        {
            _logger = logger;
            _userManager = userManager;            
        }

        public async Task<IdentityResult> Handle(
            UserCreateCommand request,
            CancellationToken cancellationToken)
        {
            var entry = new ApplicationUser
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                UserName = request.Email
            };

            var result = await _userManager.CreateAsync(entry, request.Password);
            if (result is not null) {
                if (result.Succeeded)
                {
                    result = await _userManager.AddToRoleAsync(entry, "ADMIN");
                }                    
            }            

            _logger.LogInformation("--- El EventHandler Crear usuario se ejecuto correctamente.");

            return result;
        }
    }
}
