﻿using Identity.Domain;
using Identity.Persistence.Database;
using Identity.Service.EventHandlers.Command;
using Identity.Service.EventHandlers.Response;
using IdentityModel;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace Identity.Service.EventHandlers
{
    public class UserLoginEventHandler : IRequestHandler<UserLoginCommand, IdentityAccess>
    {
        private readonly ILogger<UserLoginEventHandler> _logger;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;

        public UserLoginEventHandler(
            ILogger<UserLoginEventHandler> logger,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context,
            IConfiguration configuration)
        {
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
            _configuration = configuration;
        }
        public async Task<IdentityAccess> Handle(UserLoginCommand request, CancellationToken cancellationToken)
        {
            var result = new IdentityAccess();

            var user = await _context.Users.SingleAsync(x => x.Email == request.Email, cancellationToken);
            var response = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);

            if (response.Succeeded)
            {
                result.Succeeded = true;
                await GenerateToken(user, result);
            }
            _logger.LogInformation("--- El EventHandler para loguear al usuario se ejecuto correctamente.");
            return result;
        }

        private async Task GenerateToken(ApplicationUser user, IdentityAccess identity)
        {
            _logger.LogInformation("--- Iniciando el proceso para Generar Token.");

            var secretKey = _configuration.GetSection("SecretKey").Value;
            var key = Encoding.ASCII.GetBytes(secretKey);

            var claims = new List<Claim>
            {
                new Claim(JwtClaimTypes.Id, user.Id),
                new Claim(JwtClaimTypes.Email, user.Email),
                new Claim(JwtClaimTypes.Name, user.FirstName),
                new Claim(JwtClaimTypes.FamilyName, user.LastName)
            };

            var roles = await _context.UserRoles.Where(x => x.UserId == user.Id).Select(x => x.RoleId).ToListAsync();

            foreach (var role in roles)
            {
                var myRoles = await _context.Roles.Where(x => x.Id == role).ToListAsync();
                claims.Add(new Claim(JwtClaimTypes.Role, String.Join(",", myRoles)));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature
                )
            };


            var tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);

            identity.AccessToken = tokenHandler.WriteToken(createdToken);

            _logger.LogInformation("--- Terminando el proceso para Generar Token.");
        }
    }
}
