﻿using Identity.Domain;
using Identity.Persistence.Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.Text.Json.Serialization;
using System.Text.Json;
using Common.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using HealthChecks.UI.Client;

namespace Identity.API.Config
{
    public static class ProgramConfiguration
    {
        //builder.Services
        public static WebApplicationBuilder AddServiceControllers(this WebApplicationBuilder builder)
        {
            builder.Services.AddControllers();
            return builder;
        }
        public static WebApplicationBuilder AddJsonSerializerOptions(this WebApplicationBuilder builder)
        {
            JsonSerializerOptions serializerOptions = new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                DictionaryKeyPolicy = JsonNamingPolicy.CamelCase,
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                MaxDepth = 10,
                ReferenceHandler = ReferenceHandler.IgnoreCycles,
                WriteIndented = true
            };
            serializerOptions.Converters.Add(new JsonStringEnumConverter());
            builder.Services.AddSingleton(s => serializerOptions);
            return builder;
        }
        public static WebApplicationBuilder AddServiceMVC(this WebApplicationBuilder builder)
        {
            builder.Services.AddMvc()
            .AddMvcOptions(o => o.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter()))
            .AddJsonOptions(o =>
            {
                o.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                o.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                o.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
                o.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
                o.JsonSerializerOptions.MaxDepth = 10;
                o.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
                o.JsonSerializerOptions.WriteIndented = true;
            });
            return builder;
        }
        public static WebApplicationBuilder AddServiceEndpointsApiExplorer(this WebApplicationBuilder builder)
        {
            builder.Services.AddEndpointsApiExplorer();
            return builder;
        }
        public static WebApplicationBuilder AddServiceSwaggerGen(this WebApplicationBuilder builder)
        {
            builder.Services.AddSwaggerGen();
            return builder;
        }
        public static WebApplicationBuilder AddServiceDbContext(this WebApplicationBuilder builder)
        {
            builder.Services
                .AddDbContext<ApplicationDbContext>(
                    options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"),
                    x => x.MigrationsHistoryTable("__EFMigrationsHistory", "Identity"))
            );
            return builder;
        }
        public static WebApplicationBuilder AddServiceMediaTr(this WebApplicationBuilder builder)
        {
            builder.Services.AddMediatR(config => config.RegisterServicesFromAssembly(Assembly.Load("Identity.Service.EventHandlers")));
            return builder;
        }
        public static WebApplicationBuilder AddServiceWebHost(this WebApplicationBuilder builder)
        {
            builder.WebHost.UseUrls("http://*:10000");
            return builder;
        }
        public static WebApplicationBuilder AddServiceIdentity(this WebApplicationBuilder builder)
        {
            builder.Services
                .AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            return builder;
        }
        public static WebApplicationBuilder AddServiceIdentityConfiguration(this WebApplicationBuilder builder)
        {
            //Identity configuration
            builder.Services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;
            });
            return builder;
        }
        public static WebApplicationBuilder AddHealthChecks(this WebApplicationBuilder builder)
        {
            // health check
            builder.Services.AddHealthChecks()
                .AddCheck("self", () => HealthCheckResult.Healthy())
                .AddDbContextCheck<ApplicationDbContext>(typeof(ApplicationDbContext).Name)
                .AddSqlServer(
                connectionString: builder.Configuration.GetConnectionString("DefaultConnection"),
                healthQuery: "SELECT 1;",
                name: "sqlserver",
                failureStatus: HealthStatus.Degraded,
                tags: new string[] { "sqlserver" });           
            
            return builder;
        }

        //app
        public static WebApplication AddAppConfigure(this WebApplication app, WebApplicationBuilder builder)
        {
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                app.UseDeveloperExceptionPage();

                app.Services.GetRequiredService<ILoggerFactory>().AddSyslog(
                    builder.Configuration.GetValue<string>("Papertrail:host"),
                    builder.Configuration.GetValue<int>("Papertrail:port")
                    );
            }            

            app.UseAuthorization();
            app.UseAuthentication();

            app.MapControllers();

            app.MapHealthChecks("/hc", new HealthCheckOptions()
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            return app;
        }
    }
}

