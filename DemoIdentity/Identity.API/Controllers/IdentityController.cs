﻿using Identity.Domain;
using Identity.Service.EventHandlers.Command;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Text.Json;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace Identity.API.Controllers
{
    [ApiController]
    [Route("v1/Identity")]
    public class IdentityController : Controller
    {
        private readonly ILogger<IdentityController> _logger;        
        private readonly IMediator _mediator;

        public IdentityController(
            ILogger<IdentityController> logger,            
            IMediator mediator)
        {
            _logger = logger;            
            _mediator = mediator;
        }               

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] UserCreateCommand command)
        {
            _logger.LogInformation("Inicio del método crear");
            if (ModelState.IsValid)
            {
                var result = await _mediator.Send(command);

                if (!result.Succeeded)
                {
                    return BadRequest(result.Errors);
                }
                return Ok(result);
            }            
            return BadRequest();
        }

        [Produces("application/json", "application/xml")]
        [HttpPost("authentication")]
        public async Task<IActionResult> Authentication([FromBody] UserLoginCommand command)
        {
            _logger.LogInformation("Inicio del método autenticar");
            if (ModelState.IsValid)
            {
                var result = await _mediator.Send(command);

                if (!result.Succeeded)
                {                    
                    return BadRequest("Access denied");
                }
                                
                return Ok(result);
            }
            return BadRequest();
        }
    }
}
