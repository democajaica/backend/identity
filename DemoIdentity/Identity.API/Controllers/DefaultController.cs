﻿using Microsoft.AspNetCore.Mvc;

namespace Identity.API.Controllers
{
    [ApiController]
    [Route("/")]
    public class DefaultController : Controller
    {
        private readonly ILogger<DefaultController> _logger;

        public DefaultController(ILogger<DefaultController> logger) {
            _logger = logger;
        }       

        [HttpGet]
        public string Get() {
            _logger.LogInformation("Iniciando el servicio Identity.API");
            return "Bienvenido al API Identity";
        } 
    }
}
