using Identity.API.Config;

var builder = WebApplication.CreateBuilder(args);

builder
    .AddServiceControllers()
    .AddServiceEndpointsApiExplorer()
    .AddServiceSwaggerGen()
    .AddServiceDbContext()
    .AddServiceMediaTr()
    .AddServiceIdentity()
    .AddServiceIdentityConfiguration()
    .AddServiceWebHost()
    .AddJsonSerializerOptions()
    .AddHealthChecks();

var app = builder.Build()
    .AddAppConfigure(builder);

app.Run();